#include <vector>
#include "Vec2f.h"
#include <cassert>

/*The result is > 0 if point 'p' is left of line ab,
 * the result < 0 if it is on the right and returns 0
 * if it lies on the line.
 */
float leftOfLine(const Vec2f& a,const Vec2f& b,const Vec2f& p)
{
	/* compute the determinate of matrix 'm :
	 * | a.x a.y 1 |
	 * | b.x b.y 1 |
	 * | p.x p.y 1 |
	 * This is equivalent to |(b-a) X (p - b)|
	 */
	return a.x*b.y + a.y*p.x + b.x*p.y
	     - b.y*p.x - a.y*b.x - a.x*p.y;
}
void convexHull(const std::vector<Vec2f>& in_vs,
		std::vector<Vec2f>& out_vs)
{
	out_vs.clear();
	for(size_t i = 0; i < in_vs.size();++i)
	{
		out_vs.push_back(in_vs[i]);
		while(out_vs.size() > 2)
		{
			size_t n = out_vs.size()-1;
			Vec2f c = out_vs[n];
			Vec2f b = out_vs[n-1];
			Vec2f a = out_vs[n-2];
			if(leftOfLine(a,b,c) > 0)
			{
				break;
			}
			out_vs[n-1] = c;
			out_vs.pop_back();
		}
	}
	/*compile top of hull, exclude right most as it has already 
	 * been added and under no circumstance will it be removed.
	 * The left one has to be included though for correctly 
	 * computing the end.
	 */
	for(size_t i = in_vs.size()-1; i > 0;--i)
	{
		out_vs.push_back(in_vs[i-1]);
		while(out_vs.size() > 2)
		{
			size_t n = out_vs.size();
			Vec2f c = out_vs[n-1];
			Vec2f b = out_vs[n-2];
			Vec2f a = out_vs[n-3];
			if(leftOfLine(a,b,c) >= 0)
			{
				break;
			}
			out_vs[n-2] = c;
			out_vs.pop_back();
		}
	}
	/* remove the last point as it is redundant */
	out_vs.pop_back();
}
