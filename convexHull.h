#ifndef CONVEX_HULL_H
#define CONVEX_HULL_H

#include <vector>
#include "Vec2f.h"

void convexHull(const std::vector<Vec2f>& in_vs,
		std::vector<Vec2f>& out_vs);

#endif
