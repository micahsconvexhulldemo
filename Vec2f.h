#ifndef VECTOR_2_FLOAT_H
#define VECTOR_2_FLOAT_H

#include <cmath>
#include <iostream>

class Vec2f
{
	public:
		union
		{
			float V[2];
			struct
			{
				float x,y;
			};
		};

		Vec2f()
		{
			x = 0;
			y = 0;
		}
		Vec2f(float X,float Y)
		{
			x = X;
			y = Y;
		}

		inline bool operator==(const Vec2f &rhs) const
		{
			return (x == rhs.x && y == rhs.y);
		}

		inline bool operator!=(const Vec2f &rhs) const
		{
			return (x != rhs.x || y != rhs.y);
		}

		inline Vec2f& operator=(const Vec2f &rhs)
		{
			x = rhs.x;
			y = rhs.y;
			return *this;
		}
		inline Vec2f& operator*=(const Vec2f &param)
		{
			x *= param.x;
			y *= param.y;
			return *this;
		}
		inline Vec2f operator+ (const Vec2f& v) const
		{
			return Vec2f(x + v.x, y + v.y);
		}
		inline Vec2f operator- (const Vec2f& v) const
		{
			return Vec2f(x - v.x, y - v.y);
		}
		inline Vec2f operator* (const Vec2f& v) const
		{
			return Vec2f(x*x, y*y);
		}
		inline Vec2f operator* (float s) const
		{
			return Vec2f(x*s, y*s);
		}
		inline Vec2f operator/ (float s) const
		{
			return Vec2f(x/s, y/s);
		}
		inline Vec2f operator/ (const Vec2f& v) const
		{
			return Vec2f(x/v.x, y/v.y);
		}
		inline float Normalize()
		{
			float len = sqrtf(x*x + y*y);
			if (len > 0.0)
			{
				x /= len;
				y /= len;
			}
			return len;
		}
		inline float dot(const Vec2f& v) const
		{
			return (x*v.x + y*v.y);
		}

		inline float length() const
		{
			return sqrtf(x*x + y*y);
		}
		inline float lengthSqr() const
		{
			return x*x + y*y;
		}


		friend std::ostream& operator<< (std::ostream &lhs,const Vec2f &rhs) ;
		friend std::istream& operator>> (std::istream &lhs,const Vec2f &rhs);
};

inline float length(const Vec2f& v)
{
	return sqrtf(v.x*v.x + v.y*v.y);
}
inline float lengthSqr(const Vec2f& v)
{
	return v.x*v.x + v.y*v.y;
}

inline Vec2f floor(const Vec2f& v)
{
	return Vec2f(floor(v.x),floor(v.y));
}

inline Vec2f abs(const Vec2f& v)
{
	return Vec2f(fabs(v.x),fabs(v.y));
}

#endif
