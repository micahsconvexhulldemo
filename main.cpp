#include <gtk/gtk.h>
#include "convexHull.h"
#include <algorithm>

int main(int argc,char* argv[])
{
	gtk_init(&argc,&argv);

	GtkBuilder* builder = gtk_builder_new();
	gtk_builder_add_from_file(builder,"gui.xml",NULL);

	GtkWidget* window ;
	window = GTK_WIDGET(gtk_builder_get_object(builder,"window"));
	g_assert (window != NULL);

	/*connect signals */
	std::vector<Vec2f> points;
	gtk_builder_connect_signals (builder,&points);
	g_object_unref (G_OBJECT (builder));



	gtk_widget_show_all((window));

	gtk_main();
	return 0;
}


bool compare(const Vec2f& v1,const Vec2f& v2)
{
	if(v1.x < v2.x)
		return true;
	else if (v1.x == v2.x)
		return (v1.y > v2.y);
	return false;
}
	
extern "C"
{
 gboolean drawarea_button_press_event_cb(GtkWidget* widget,
			     GdkEventButton* button,
			     std::vector<Vec2f>* points)
{
	points->push_back(Vec2f(button->x,button->y));

	gtk_widget_queue_draw(widget);
	return FALSE;

}

 gboolean drawarea_expose_event_cb(GtkWidget* widget,
				      GdkEventExpose* event,
				      std::vector<Vec2f>* points)
{
	if(!points->empty())
	{

		std::sort(points->begin(),points->end(),&compare);
		std::vector<Vec2f> CH;
		convexHull(*points,CH);

		gdk_window_clear(widget->window);

		cairo_t* cr = gdk_cairo_create(widget->window);



		/* draw polygon that is representative of the convex hull*/
		cairo_move_to(cr,CH[0].x,CH[0].y);
		for(size_t i = 1;i < CH.size();++i)
			cairo_line_to(cr,CH[i].x,CH[i].y);
		cairo_line_to(cr,CH[0].x,CH[0].y);

		cairo_set_source_rgb(cr,0,0,0);
		cairo_stroke(cr);

		/* draw all the points */
		for(size_t i = 0;i < points->size();++i)
			cairo_rectangle(cr,(*points)[i].x,(*points)[i].y, 2,2);
		cairo_set_source_rgb(cr,1,0,0);
		cairo_stroke(cr);

		cairo_destroy(cr);

	}

	return FALSE;
}


 void clear(GtkWidget* widget,
		  std::vector<Vec2f>* points)
{
	points->clear();
}
gboolean window_destroy_cb(GtkWidget* widget,
				      gpointer*)
{
	gtk_main_quit();
	return false;
}


}
